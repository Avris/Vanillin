const $tests = document.querySelector('.tests');

eq = (a, b) => {
    if (JSON.stringify(a) === JSON.stringify(b)) {
        return true;
    }

    console.error('Expected:', b, 'Actual:', a);

    return false;
};

let alertShown = false;

test = (name, run) => {
    const $el = _el(`<li class="test" data-name="${name}" data-result="running"><span class="test-result">✵</span> ${name}</li>`);
    $tests.appendChild($el);

    const ok = () => {
        $el.querySelector('.test-result').innerHTML = '✔';
        $el.dataset.result = 'ok';
    };

    const fail = (message) => {
        console.error('Fail', name, message);
        $el.querySelector('.test-result').innerHTML = '✘';
        $el.dataset.result = 'fail';
        if (!alertShown) {
            alert('Some tests have failed, check the console.');
            alertShown = true;
        }
    };

    try {
        run((assertion) => assertion ? ok() : fail(), ok, fail);
    } catch (e) {
        console.error(e);
        fail();
    }
};

// -----------------

test('each-array', (assert) => {
    const out = _each(['a', 'b', 'c'], (el, i) => [el, i]);
    assert(eq(out, [['a', '0'], ['b', '1'], ['c', '2']]));
});

test('each-object', (assert) => {
    const out = _each({'f0': 'a', 'f1': 'b', 'f2': 'c'}, (el, i) => [el, i]);
    assert(eq(out, [['a', 'f0'], ['b', 'f1'], ['c', 'f2']]));
});

test('each-element', (assert) => {
    const out = _each(document.querySelectorAll('.foo'), (el, i) => [el.innerText, i]);
    assert(eq(out, [['aa', '0'], ['bb', '1'], ['cc', '2']]));
});

test('each-scalar', (assert) => {
    const out = _each(8, (el, i) => [el.innerText, i]);
    assert(eq(out, []));
});

// -----------------

test('el', (assert, ok, fail) => {
    const $el = _el('<div class="lorem"><a href="/test">OK</a></div>');
    if (!$el instanceof HTMLElement) {
        fail('$el not instanceof HTMLElement');
    }
    if ($el.tagName !== 'DIV') {
        fail('$el not a div');
    }
    if (!eq(Array.from($el.classList), ['lorem'])) {
        fail();
    }
    if ($el.children.length !== 1) {
        fail('Unexpected number of $el children: ' + $el.children.length);
    }
    if ($el.firstChild.tagName !== 'A') {
        fail('$el child not an A');
    }
    if ($el.innerText !== 'OK') {
        fail('$el text not OK');
    }
    ok();
});

// -----------------

test('sel', (assert) => {
    assert(_sel('.workspace') === document.querySelector('.workspace'));
});

test('sel-withStart', (assert) => {
    const $ul = _sel('ul', _sel('.workspace'));
    assert($ul.parentNode.classList.contains('bar-parent'));
});

test('selAll', (assert) => {
    assert(_selAll('div').length === 10);
});

test('selAll-withStart', (assert) => {
    assert(_selAll('div', _sel('.workspace')).length === 8);
});

// -----------------

test('findParent', (assert) => {
    const $expectedParent = document.querySelectorAll('.bar-parent')[2];
    const $el = document.querySelector('.bar');
    assert(_findParent($el, '.bar-parent') === $expectedParent);
});

test('findParent-not-found', (assert) => {
    const $el = document.querySelector('.bar');
    assert(_findParent($el, '.nope') === undefined);
});

// -----------------

test('on', (assert) => {
    const $el = document.querySelector('.workspace .dynamic');
    const $child1 = _el('<a href="#a1" class="catch-click"></a>');
    const $child2 = _el('<a href="#a2"></a>');
    const $child3 = _el('<a href="#a3" class="catch-click"></a>');
    const $grandChild3 = _el('<span>A3-sub</span>');
    $child3.appendChild($grandChild3);
    const $child4 = _el('<a href="#a4"></a>');
    const $grandChild4 = _el('<span>A4-sub</span>');
    $child4.appendChild($grandChild4);

    const out = [];

    $el.appendChild($child1);
    $el.appendChild($child2);
    _on('.workspace .dynamic .catch-click', 'click', function (e) {
        out.push(this.attributes.href.value);
        return false;
    });
    $el.appendChild($child3);
    $el.appendChild($child4);

    const event = document.createEvent('HTMLEvents');
    event.initEvent('click', true, false);

    $child1.dispatchEvent(event);
    $child2.dispatchEvent(event);
    $child3.dispatchEvent(event);
    $grandChild3.dispatchEvent(event);
    $child4.dispatchEvent(event);
    $grandChild4.dispatchEvent(event);

    assert(eq(out, ['#a1', '#a3', '#a3']))
});

test('on-node', (assert) => {
    const $elYes = document.querySelector('#on-test-yes');
    const $elNo = document.querySelector('#on-test-no');

    const out = [];

    _on($elYes, 'click', function (e) {
        out.push(this.dataset.value);
        return false;
    });

    const event = document.createEvent('HTMLEvents');
    event.initEvent('click', true, false);

    $elYes.dispatchEvent(event);
    $elNo.dispatchEvent(event);

    assert(eq(out, ['Y']))
});

test('on-multiple', (assert) => {
    const $el = document.querySelector('#on-test-multiple');

    const out = [];

    _on($el, ['keydown', 'keyup'], function (e) {
        out.push(this.dataset.value);
        return false;
    });

    const createEvent = function (type) {
        const event = document.createEvent('HTMLEvents');
        event.initEvent(type, true, false);
        return event;
    };

    $el.dispatchEvent(createEvent('keydown'));
    $el.dispatchEvent(createEvent('keyup'));
    $el.dispatchEvent(createEvent('click'));

    assert(eq(out, ['OK', 'OK']))
});

// -----------------

test('request-get', (assert, ok, fail) => {
    _request('GET', '/get')
        .then((data) => assert(eq(JSON.parse(data), {header: '-'})))
        .catch(() => fail());
});

test('request-get-header', (assert, ok, fail) => {
    _request('GET', '/get', {}, {'x-test': 'content'})
        .then((data) => assert(eq(JSON.parse(data), {header: 'content'})))
        .catch(() => fail());
});

test('request-post-form', (assert, ok, fail) => {
    _request('POST', '/post', document.querySelector('form'))
        .then((data) => assert(eq(JSON.parse(data), {FOO: ['FOO'], 'SELECT[]': ['A1', 'A3']})))
        .catch((xhr) => fail(xhr));
});

test('request-post-formdata', (assert, ok, fail) => {
    _request('POST', '/post', new FormData(document.querySelector('form')))
        .then((data) => assert(eq(JSON.parse(data), {FOO: ['FOO'], 'SELECT[]': ['A1', 'A3']})))
        .catch((xhr) => fail(xhr));
});

test('request-post-object', (assert, ok, fail) => {
    _request('POST', '/post', {lorem: 'ipsum', foo: ['phoebe', 'buffay']})
        .then((data) => assert(eq(JSON.parse(data), {LOREM: ['IPSUM'], FOO: ['PHOEBE,BUFFAY']})))
        .catch((xhr) => fail(xhr));
});

test('request-not-found', (assert, ok, fail) => {
    _request('GET', '/nope')
        .then((data, xhr) => fail(xhr))
        .catch((xhr) => assert(eq(xhr.status, 404)));
});

test('request-method-not-allowed', (assert, ok, fail) => {
    _request('GET', '/post')
        .then((data, xhr) => fail(xhr))
        .catch((xhr) => assert(eq(xhr.status, 405)));
});

test('request-refused', (assert, ok, fail) => {
    _request('GET', '//nonexistent.domain.hopefully/origin')
        .then((data, xhr) => fail(xhr))
        .catch((xhr) => ok());
});

// -----------------

test('extend', (assert) => {
    const objA = {foo: 5, 'bar': {lorem: 1, ipsum: 2}};
    const objB = {foo: 0, 'bar': {lorem: 0}, baz: 'xxx'};
    const expected = {foo: 0, bar: {lorem: 0}, baz: 'xxx'};

    assert(eq(
        _extend({}, objA, objB),
        expected
    ));
});

export const _each = (obj, callable) => {
    const ret = [];

    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            ret.push(callable(obj[key], key));
        }
    }

    return ret;
};

export const _el = (html) => {
    const $div = document.createElement('div');
    $div.innerHTML = html.trim();

    return $div.firstChild;
};

export const _sel = (selector, startNode = document) => startNode.querySelector(selector);

export const _selAll = (selector, startNode = document) => [...startNode.querySelectorAll(selector)];

export const _findParent = ($el, selector) => {
    do {
        if ($el.matches(selector)) {
            return $el;
        }
        $el = $el.parentNode;
    } while ($el && $el.matches);
};

export const _on = (selector, events, handler) => {
    const execEvent = (target, e) => {
        const result = handler.apply(target, [e]);
        if (result === false) {
            e.preventDefault();
            e.stopPropagation();
        }
    };

    if (typeof events === 'string') {
        events = [events];
    }

    _each(events, event => {
        if (selector instanceof Node) {
            selector.addEventListener(event, (e) => execEvent(selector, e));

            return;
        }

        document.addEventListener(event, (e) => {
            const match = _findParent(e.target, selector);
            if (match) {
                execEvent(match, e);
            }
        });
    });
};

export const _request = (method, url, data, headers) => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open(method, url, true);
        _each(headers || {}, (val, key) => xhr.setRequestHeader(key, val));
        xhr.onload = () => {
            if (xhr.status >= 400) {
                reject(xhr);
            } else {
                resolve(xhr.responseText, xhr);
            }
        };
        xhr.onerror = () => reject(xhr);

        let formData;
        if (data instanceof HTMLElement) {
            formData = new FormData(data);
        } else if (data instanceof FormData) {
            formData = data;
        } else {
            formData = new FormData();
            _each(data, (val, key) => formData.append(key, val));
        }

        xhr.send(formData);
    });
};

export const _extend = (out, ...args) => {
    out = out || {};

    for (let i = 1; i < args.length; i++) {
        const obj = args[i];
        if (!obj) {
            continue;
        }

        _each(obj, (val, key) => {
            out[key] = val;
        });
    }

    return out;
};

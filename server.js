const http = require('http');
const fs = require('fs');
const multiparty = require('multiparty');

http.createServer(function (req, res) {
    switch (req.url) {
        case '/':
            fs.readFile('tests.html', function(err, data) {
                res.writeHead(200, {'Content-Type': 'text/html'});
                res.write(data);
                res.end();
            });
            break;
        case '/get':
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({header: req.headers['x-test'] || '-'}));
            res.end();
            break;
        case '/post':
            if (req.method !== 'POST') {
                res.writeHead(405, {'Content-Type': 'text/plaintext'});
                res.write('Method not allowed');
                res.end();
            } else {
                const form = new multiparty.Form();
                form.parse(req, function(err, fields, files) {
                    res.writeHead(200, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify(fields).toUpperCase());
                    res.end();
                });
            }
            break;
        default:
            fs.readFile(req.url.substr(1), function(err, data) {
                if (err) {
                    res.writeHead(404);
                    res.write('Not found');
                    res.end();
                } else {
                    res.writeHead(200);
                    res.write(data);
                    res.end();
                }
            });
    }
}).listen(8777);

# Vanillin.js – Almost like vanilla JavaScript

![Logo](img/logo.png)

[jQuery](https://jquery.com/) used to be virtually indispensable,
if you wanted to develop a cross-browser website without getting a headache.

Today, however, [you might not need jQuery](http://youmightnotneedjquery.com/),
especially, if you're developing a library and want to avoid unnecessary dependencies.

Still, some helpers could be useful...
Vanillin is an opinionated set of helpers
that I find most useful, a bare minimum to make life easier.  

## Installation

You can either install Vanillin as a node module:

    $ npm i --save avris-vanillin
    
    or
    
    $ yarn add avris-vanillin
    
    require('avris-vanillin');

Use a CDN:

    <script src="https://glcdn.githack.com/Avris/Vanillin/raw/v0.2.0/dist/Vanillin.min.js"></script>
    
Or simply copy-paste whatever filters you need from the [source code](https://gitlab.com/Avris/Vanillin/blob/v0.0.2/src/Vanillin.js).

## Usage

### `_each(iterable, function (el, i))`

Iterates in a consistent way, whether it's over an array or an object,
without having to check for `hasOwnProperty` in every loop.

    _each(object, function (val, key) {
        // ...
    }); 
    
`_each` returns an array of values that you have (optionally) returned from the callback.

### `_el(html)`

Converts an HTML string into a DOM element with one simple helper.

    const el = _el('<div class="lorem"><a href="/test">OK</a></div>');
    
### `_sel(selector, [startNode])`

Alias for `querySelector`:

    const el = _sel('.card img.avatar')

### `_selAll(selector, [startNode])`

Alias for `querySelectorAll`:

    _each(_selAll('.card img.avatar'), function (el) { 
        el.classList.add('active')
    })

### `_findParent(el, selector)`

Finds the first parent of `el` that matches `selector`.

    parent = _findParent(el, 'section.important')
    
### `_on(selector, event, handler (e))`

Attaches the `handler` as an event listener or `event` for all elements that match `selector`,
even if they don't exist yet at that moment.

    _on('a.foo', 'click', function (e) {
        console.log('foo link clicked', this.attributes.href.value)
    });
    
You can also pass a specific DOM Element instead of a selector, to only add a listener to this instance.

You can also attach the handler to multiple events: `_on('a.foo', ['click', 'change', 'keyup'], e => console.log(e)`.

`this` in the callback is the element you have selected for,
 and it might be different from `e.target`, which might be its child.
 
Returning `false` from the callback will stop propagation and prevent default. 
    
### `_request(method, url, data, headers)`

Performs an AJAX request to `url` using a specified HTTP `method`,
attaching `data` (they can be either `FormData`, an object, or a DOM element containing form data)
and using specified `headers`. Returns a `Promise`.

    _request('POST', '/document/create', document.querySelector('form'), {Authorization: 'Basic YWxhZGRpbjpvcGVuc2VzYW1l'})
        .then((data) => {
            alert('Document created with ID: ' + JSON.parse(data).id);
        })
        .catch((xhr) => {
            console.log(xhr);
            alert('Error while creating a document');
        };

### `_extend(out, ...args)`

Extends an object `out` with data from `...args`, useful for resolving configs:

    function Helper(options) {
        this.config = _extend({}, defaultOptions, options);
    }

## Development

To install dev dependencies:

    yarn

To build a minified, babel-ified version:

    yarn build

To run tests:

    yarn test
    
and open [http://localhost:8777](http://localhost:8777).

## Copyright
 
 * **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
 * **Licence:** [MIT](https://mit.avris.it)
 * **Logo**: [Sbrools](https://en.wikipedia.org/wiki/Vanillin#/media/File:Vanillin-3d.png) (CC-BY)
